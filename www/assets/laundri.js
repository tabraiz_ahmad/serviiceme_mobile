window.laundryOptions=  [{
  "name": "Kurta Pajama / Salwar Suit",
  "options": [{
    "option": "Wash and Iron"
  }, {
    "option": "Dry Clean"
  }, {
    "option": "Iron"
  }]
}
  , {
    "name": "Salwar Khamis 3pc ",
    "options": [{
      "option": "Wash and Iron"
    }, {
      "option": "Dry Clean"
    }, {
      "option": "Iron"
    }]
  }, {
    "name": "Abaya  / Saree ",
    "options": [{
      "option": "Wash and Iron"
    }, {
      "option": "Dry Clean"
    }, {
      "option": "Iron"
    }]
  }, {
    "name": "Ghutra",
    "options": [{
      "option": "Wash and Iron"
    }, {
      "option": "Dry Clean"
    }, {
      "option": "Iron"
    }]
  }, {
    "name": "Kandura",
    "options": [{
      "option": "Wash and Iron"
    }, {
      "option": "Dry Clean"
    }, {
      "option": "Iron"
    }]
  }, {
    "name": "Sherwani 2 pc",
    "options": [{
      "option": "Wash and Iron"
    }, {
      "option": "Dry Clean"
    }, {
      "option": "Iron"
    }]
  }, {
    "name": "Lungi",
    "options": [{
      "option": "Wash and Iron"
    }, {
      "option": "Dry Clean"
    }, {
      "option": "Iron"
    }]
  }, {
    "name": "Sirwal",
    "options": [{
      "option": "Wash and Iron"
    }, {
      "option": "Dry Clean"
    }, {
      "option": "Iron"
    }]
  }, {
    "name": "Dress Shirt",
    "options": [{
      "option": "Wash and Iron"
    }, {
      "option": "Dry Clean"
    }, {
      "option": "Iron"
    }]
  }, {
    "name": "Dress Shirt",
    "options": [{
      "option": "Wash and Iron"
    }, {
      "option": "Dry Clean"
    }, {
      "option": "Iron"
    }]
  }, {
    "name": "Dress Shirt",
    "options": [{
      "option": "Wash and Iron"
    }, {
      "option": "Dry Clean"
    }, {
      "option": "Iron"
    }]
  }, {
    "name": "Dress Shirt",
    "options": [{
      "option": "Wash and Iron"
    }, {
      "option": "Dry Clean"
    }, {
      "option": "Iron"
    }]
  }, {
    "name": "Laundered Shirt Launder & Press",
    "options": [{
      "option": "Wash and Iron"
    }, {
      "option": "Dry Clean"
    }, {
      "option": "Iron"
    }]
  }, {
    "name": "Sweatshirt",
    "options": [{
      "option": "Wash and Iron"
    }, {
      "option": "Dry Clean"
    }, {
      "option": "Iron"
    }]
  }, {
    "name": "Sweaters",
    "options": [{
      "option": "Wash and Iron"
    }, {
      "option": "Dry Clean"
    }, {
      "option": "Iron"
    }]
  }, {
    "name": "Kids Dresses",
    "options": [{
      "option": "Wash and Iron"
    }, {
      "option": "Dry Clean"
    }, {
      "option": "Iron"
    }]
  }, {
    "name": "Nightgown",
    "options": [{
      "option": "Wash and Iron"
    }, {
      "option": "Dry Clean"
    }, {
      "option": "Iron"
    }]
  }, {
    "name": "Gown",
    "options": [{
      "option": "Wash and Iron"
    }, {
      "option": "Dry Clean"
    }, {
      "option": "Iron"
    }]
  }, {
    "name": "Dresses",
    "options": [{
      "option": "Wash and Iron"
    }, {
      "option": "Dry Clean"
    }, {
      "option": "Iron"
    }]
  }, {
    "name": "Jeans",
    "options": [{
      "option": "Wash and Iron"
    }, {
      "option": "Dry Clean"
    }, {
      "option": "Iron"
    }]
  }, {
    "name": "Pants",
    "options": [{
      "option": "Wash and Iron"
    }, {
      "option": "Dry Clean"
    }, {
      "option": "Iron"
    }]
  }, {
    "name": "Outerwear - Small",
    "options": [{
      "option": "Dry Clean"
    }]
  }, {
    "name": "Outerwear - Large",
    "options": [{
      "option": "Dry Clean"
    }]
  }, {
    "name": "Cardigans",
    "options": [{
      "option": "Wash and Iron"
    }, {
      "option": "Dry Clean"
    }, {
      "option": "Iron"
    }]
  }, {
    "name": "Jackets",
    "options": [{
      "option": "Wash and Iron"
    }, {
      "option": "Dry Clean"
    }, {
      "option": "Iron"
    }]
  }
]
