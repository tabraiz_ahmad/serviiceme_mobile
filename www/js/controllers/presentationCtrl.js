application.controller('presentationCtrl',[ '$scope','$state','$stateParams','Session', function ($scope, $state, $stateParams,Session) {

  var init = function(){
    $scope.type = $stateParams.type;
    $scope.navTitle = "Service Me";
    $scope.hideBackButton = true;

    if($scope.type == 'client'){
      Session.setItem('client_present_shown',true);
    }
    if($scope.type == 'supplier'){
      Session.setItem('supplier_present_shown',true);
    }
  }

  init();
}
]);
