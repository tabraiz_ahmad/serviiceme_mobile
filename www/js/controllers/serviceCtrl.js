application.controller('serviceCtrl',['$scope','$rootScope','$state','Session',function($scope, $rootScope,$state, Session){
	var init = function(){
		var serviceType = "Home Services";
		$scope.navTitle = serviceType;
		$scope.hideBackButton = true;
		$scope.options = {};
  }
	$scope.createOffer = function(category){
		if(category){
				$state.go('menu.offerMethod',{category:category});
		}else{
			$state.go('dev');
		}
	}
  $scope.showHide = function(event, val){
    $scope.options[val] = $scope.options[val] ? false : true;
    event.stopPropagation();
  }
  init();
}
])
