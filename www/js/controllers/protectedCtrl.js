application.controller('protectedCtrl',['$scope', '$state', 'Session', function ($scope, $state, Session) {

  //checking user session,
  // also need to implement if connection to server exist
  var init = function(){
    $scope.hideBackButton = true;
    var user = Session.getItemObject('current_user_data');

    if (user  && user.profile_completed) {
      $state.go('menu.landing');
    }
  }
  $scope.presentScreen = function(type){
      if(type == 'client'){
        var client_present_shown = Session.getItem('client_present_shown');
        if(client_present_shown){
            $state.go('login',{'type':type});
        }else{
            $state.go('presentation',{'type':type});
        }
      }else if(type == 'supplier'){
        var supplier_present_shown = Session.getItem('supplier_present_shown');
        if(supplier_present_shown){
            $state.go('login',{'type':type});
        }else{
            $state.go('presentation',{'type':type});
        }
      }else{
          $state.go('presentation',{'type':type});
      }


  }
  init();
}
]);
