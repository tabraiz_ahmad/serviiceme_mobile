application.controller('loginCtrl',['$scope','$rootScope', '$state', '$stateParams', '$ionicActionSheet','$ionicModal','User', 'Utils','Session','Client' ,function($scope,$rootScope, $state, $stateParams, $ionicActionSheet, $ionicModal, User, Utils, Session, Client) {
  var init = function(){
    $scope.navTitle = 'LogIn';
    $scope.hideBackButton = true;
    $scope.type = $stateParams.type;

  }
  $scope.user = {};
  $scope.fbLogin = function() {
    Utils.showLoading();
    if(facebookConnectPlugin){
      facebookConnectPlugin.getLoginStatus(function(success) {
        if (success.status === 'connected') {
          // The user is logged in and has authenticated app, and response.authResponse supplies
          // the user's ID, a valid access token, a signed request, and the time the access token
          // and signed request each expire
          var auth_token = success.authResponse.accessToken;
          var auth_promise = User.auth($scope.type,'facebook',auth_token);
          auth_promise.success(function(response){
            Utils.hideLoading();
            //login successfully save in session
            Utils.debug("fb login", response.user, response.token, response);
            $rootScope.user = response.user;
            Session.setItemObject('current_user_data',response.user);
            Session.setItem('token',response.token);
            $state.go('menu.landing');
          }).error(function(error){
            Utils.hideLoading();
            Utils.debug("Error while login facebook" + error);
            Utils.alertPopup('Login Error','Please try later');
          });

        } else {
          facebookConnectPlugin.login(['email', 'public_profile'], fbLoginSuccess, fbLoginError);
        }
      })
    }else{
      Utils.debug('not mobile, unable to get facebookConnectPlugin');
    }

  }

  // This is the success callback from the login method
  var fbLoginSuccess = function(response) {
    if (!response.authResponse) {
      fbLoginError("Cannot find the authResponse");
      return;
    }
    var auth_token = response.authResponse;
    var auth_promise = User.auth($scope.type,'facebook',auth_token);
    auth_promise.success(function(response){
      Utils.hideLoading();
      Utils.debug(response);
      $rootScope.user = response.user;
      Session.setItemObject('current_user_data',response.user);
      Session.setItem('token',response.token);
      $state.go('menu.landing');

    }).error(function(error){
      Utils.hideLoading();
      Utils.debug("Unable to send token to serve: "+ error);
      Utils.alertPopup('Login Error','Please try later');
    });
  };

  // *******************************************
  // This is the fail callback from the login method
  var fbLoginError = function(error) {
    Utils.debug('fbLoginError', error);
    Utils.hideLoading();
  };

  $scope.signIn = function() {
    Utils.showLoading();
    var promise_login = User.signIn($scope.user);
    promise_login.success(function(response){
      Utils.hideLoading();
      $rootScope.user = response.user;
      Session.setItemObject('current_user_data',response.user);
      Session.setItem('token',response.token);
      $state.go('menu.landing');
    }).error(function (error){
      Utils.hideLoading();
      Utils.debug("user login",error)
      Utils.alertPopup('Login Error',error.message);

    });
  }

  // ********************************************
  // Login With googleplus
  $scope.gPlusLogin = function() {

    Utils.showLoading();
    window.plugins.googleplus.login(
      {
        'offline': true
      },
      function (user_data) {
        if(user_data.oauthToken){
          var auth_promise = User.auth($scope.type,'google',user_data.oauthToken);
          auth_promise.success(function(response){
            Utils.hideLoading();
            $rootScope.user = response.user;
            Session.setItemObject('current_user_data',response.user);
            Session.setItem('token',response.token);
            $state.go('menu.landing');

          }).error(function(error){
            Utils.hideLoading();
            Utils.debug("send token error",error);
          });
        }else{
          Utils.debug("Unable to get Gplus token",user_data);
        }
        Utils.hideLoading();
      },
      function (msg) {
        Utils.hideLoading();
      }
    );
  };

  //signup window
  $scope.signup_window = function () {
    $scope.user={};
    $ionicModal.fromTemplateUrl('templates/modals/signupStep1.html', function (modal) {
      $scope.signup_modal = modal;
      $scope.signup_modal.show();
    }, {
      scope: $scope
    });
  };
  $scope.confirm_password_key_up = function(user){
    var v = user.password===user.password_confirmation;
    $scope.pwError = !v;
  }
  //signup window
  $scope.signup_step_2 = function () {
    $scope.user = Session.getItemObject('current_user_data');

    $ionicModal.fromTemplateUrl('templates/modals/signupStep2.html', function (modal) {
      $scope.signup_step2_modal = modal;
      $scope.signup_step2_modal.show();
    }, {
      scope: $scope
    });
  };
  $scope.signUpStep2Form = function(){
    $scope.showPassword = false;
    Utils.showLoading();
    var data = $scope.user;
    data.from_mobile = true;
    data.step = 2;
    if($scope.type == 'client'){
      var update_client_promie = Client.update(data);
      update_client_promie.success(function(response){
        Utils.hideLoading();
        Utils.debug("successfully registered",response);
        Session.setItemObject('current_user_data',response.client);
        //save response to user
        $scope.cancel_signup_step2_window();
        $state.go('menu.landing');

      }).error(function(error){
        Utils.hideLoading();
        Utils.alertPopup('Error',error);
        Utils.debug("unable to update",error);
      });

    }else if ($scope.type == 'supplier'){
      Utils.alertPopup("Supplier Sign Up","Under development");
    }

  }
  //
  $scope.create_user = function(){
    Utils.showLoading();
    var data = $scope.user;
    data.from_mobile = true;
    data.step = 1;
    if($scope.type == 'client'){
      var create_client_promie = Client.create(data);
      create_client_promie.success(function(response){
        Utils.hideLoading();
        Utils.debug("successfully registered",response);
        var user = {};
        if(response.id){
          user.id = response.id;
        }
        else{
          user = response.client;
        }
        Session.setItemObject('current_user_data',user);
        //save response to user
        $scope.cancel_signup_window();
        $scope.verify_num_window();

      }).error(function(error){
        Utils.hideLoading();
        Utils.alertPopup('SignUp Error',error);
        Utils.debug("unable to create",error);
      });

    }else if ($scope.type == 'supplier'){
      Utils.alertPopup("Supplier Sign Up","Under development");
    }

  }
  //verification window
  $scope.verify_num_window = function () {
    $ionicModal.fromTemplateUrl('templates/modals/verify_num.html', function (modal) {
      $scope.verification = modal;
      $scope.verification.show();
    }, {
      scope: $scope
    });
  };

  $scope.cancel_signup_window = function(){
    $scope.signup_modal.remove();
  }
  $scope.cancel_signup_step2_window = function(){
    $scope.signup_step2_modal.remove();
  }

  $scope.cancel_verify_num = function(){
    $scope.verification.remove();
  }

  $scope.verfiy_user_num = function(){
    console.log($scope.user.code);
    var user = Session.getItemObject('current_user_data');
    var id = user.id;
    Utils.showLoading();
    var data = {};
    data.code = $scope.user.code;
    var verify_user_promise = User.verifyCode(id,data);
    verify_user_promise.success(function(response){
      Utils.hideLoading();
      Utils.debug("successfully registered",response);
      $scope.cancel_verify_num();
      Session.setItemObject('current_user_data',response.user);
      Session.setItemObject('token',response.token);

      if(response.user.profile_completed){
          $state.go('menu.landing');
      }else{
        //save response to user
        $scope.signup_step_2();

      }

    }).error(function(error){
      Utils.hideLoading();
      Utils.debug("num not verified",error);
      Utils.alertPopup("Error",error.message);
    });
  }
  // ********************************************
  // Resend action sheet

  $scope.showResendMenu = function() {
    var hideSheet = $ionicActionSheet.show({
      buttons: [
        { text: '<i class="icon ion-ios-reload balanced"></i> Resend SMS ' },
        { text: '<i class="icon ion-edit energized"></i>Change phone number' }
      ],
      titleText: "Didn't recieve SMS?",
      cancelText: 'Cancel',
      cancel: function() {},
      buttonClicked: function(index) {
        hideSheet();
        if(index === 0) {

          // Resend SMS
          var user = Session.getItemObject('current_user_data');
          var id = user.id;
          Utils.showLoading();
          var resend_code_promise = User.resendCode(id);
          resend_code_promise.success(function(response){
            Utils.hideLoading();
          }).error(function(error){
            Utils.hideLoading();
            Utils.debug("unable to send",error);
            Utils.alertPopup("Error",error.message);
          });
        }

        if(index === 1) {
          // Change phone number
          $scope.user.code="";
          $scope.user.phone="";
          $scope.cancel_verify_num();
          $scope.signup_window();
        }
      },

      destructiveButtonClicked: function() {
        // add delete code..
      }
    });
  };
  init();
}
])
