application.controller('menuCtrl',['$scope', '$rootScope', '$state','Session', function($scope, $rootScope, $state,Session) {
var init = function(){
  $scope.logged = false;
  $rootScope.user = Session.getItemObject('current_user_data');
  if($rootScope.user){
    $scope.logged = true;
  }
}
$scope.login = function(){
  $state.go('login');
}
$scope.logout = function(){
  Session.clearSession();
  $state.go('login');
}

init();
}
]);
