application.controller('offerMethodCtrl',['$scope','$state','$stateParams',function($scope,$state,$stateParams){
	var init = function(){
		$scope.category = $stateParams.category;
	}
	$scope.bid = function(){
		$state.go('menu.offer',{category: $scope.category,type:'bid'});
	}
	$scope.direct = function(){
		$state.go('dev');
	}

  init();
}
])
