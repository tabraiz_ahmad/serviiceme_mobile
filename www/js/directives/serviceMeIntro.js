application.directive('serviceMeIntro', function () {
  return {
    templateUrl: function (tElement, tAttrs) {
      if (tAttrs) {
        if (tAttrs.type === 'supplier') {
          return 'templates/directives/supplierPresentation.html';
        }else{
            return 'templates/directives/clientPresentation.html';
        }
      }
      return 'templates/directives/clientPresentation.html';
    }

  }
});
