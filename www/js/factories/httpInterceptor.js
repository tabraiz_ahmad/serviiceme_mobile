application.factory('httpInterceptor',[ 'localStorageService', '$rootScope','$injector', function (localStorageService, $rootScope, $injector) {
  var httpInterceptor = {};
  httpInterceptor.request = function (config) {
      config.headers = config.headers || {};
      if(localStorageService.get('token'))
      config.headers['token'] = localStorageService.get('token');
      config.timeout = 60000;
      return config;
    },
    httpInterceptor.response = function (response) {
      return response;
    }

    // httpInterceptor.requestError = function(response){
    //   return response;
    // },
    // httpInterceptor.responseError = function (response) {
    //   //Utils.hideLoading();
    //   if (response.status == 401) {
    //     $rootScope.$broadcast('SERVICEME:unauthorized');
    //   }
    //   if (response.status == 0) {
    //     $rootScope.$broadcast('SERVICEME:requestTimeout');
    //   }
    //   return response;
    // }
    return httpInterceptor;
}
]);
