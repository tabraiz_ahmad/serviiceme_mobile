var application = angular.module('serviceMe', ['ionic','LocalStorageModule','ServiceMe.Constants','intlpnIonic'])

.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    if(window.cordova && window.cordova.plugins.Keyboard) {

      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);
    }
    if(window.StatusBar) {
      StatusBar.styleDefault();
    }
  });
}).config(function ($httpProvider, localStorageServiceProvider, $ionicConfigProvider) {
  $ionicConfigProvider.backButton.text('').icon('ion-chevron-left').previousTitleText(false);
  localStorageServiceProvider
          .setPrefix('serviceMe')
          .setStorageType('localStorage')
          .setNotify(true, true);
  $httpProvider.interceptors.push('httpInterceptor');
})
