application.factory('User',['$http','config', function($http,config) {
    var service={};
    service.auth = function(type,provider,auth_token) {
      return $http.post(config.API_BASE_PATH + '/users/auth/' + type + '/' + provider,{'access_token': auth_token});
    };
    service.signIn = function(data) {
      return $http.post(config.API_BASE_PATH + '/users/sign_in', data);
    };

    service.verifyCode = function(id, data) {
      return $http.post(config.API_BASE_PATH + '/users/' + id + '/verify_code', data);
    };

    service.resendCode = function(id) {
      return $http.get(config.API_BASE_PATH + '/users/' + id + '/resend_code');
    };
    return service;
}
]);
