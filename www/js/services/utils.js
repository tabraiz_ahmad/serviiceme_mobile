application.factory('Utils',['$ionicPopup','config','$ionicLoading', function ($ionicPopup,config, $ionicLoading) {
  var utils = {};

  // ********************************************
  // A confirm dialog, returing promise
  utils.confirmPopup = function(title, message) {
    var confirm_popup_promise = $ionicPopup.confirm({
      title: title,
      template: message
    });
    return confirm_popup_promise;
  };

  // ********************************************
  utils.alertPopup = function(title, message){
    var alert_popup = $ionicPopup.alert({
      title: title,
      template: message
    });
    return alert_popup;

  }

  // ********************************************
  utils.showLoading = function(){
    $ionicLoading.show({
      template: '<ion-spinner icon="ripple"></ion-spinner>'
    });
  }
  // ********************************************
  utils.hideLoading = function(){
    $ionicLoading.hide();
  }

  // ********************************************
  utils.debug = function () {
    var args = arguments; // allow comma separated debug
    // messages

    // output messages to console
    if (config.ENVIROMENT === 'dev') {
      angular.forEach(args, function (msg, key) {
        console.log(msg);
      });
    }
  };


  return utils;
}
]);
