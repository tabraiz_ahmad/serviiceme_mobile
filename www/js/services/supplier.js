application.factory('Supplier',['$http','config', function($http,config) {
    var service={};

    service.create = function(supplier) {
      return $http.post(config.API_BASE_PATH + '/suppliers', { supplier: supplier });
    };

    service.update = function(supplier) {
      return $http.put(config.API_BASE_PATH + '/suppliers/' + client.id, { supplier: supplier });
    };

    service.destroy = function(supplier) {
      return $http.delete(config.API_BASE_PATH + '/suppliers/' + supplier.id);
    };

    service.find = function(supplier) {
      return $http.get(config.API_BASE_PATH + '/suppliers/' + supplier.id);
    };
    return service;
}
]);
