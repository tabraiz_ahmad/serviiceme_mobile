application.factory('Session', ['localStorageService',function(localStorageService) {
    var session = {};

    session.setItem = function(key,value){
        localStorageService.set(key,value);
    };

    session.getItem = function(key){
        return localStorageService.get(key);
    };

    session.setItemObject = function(key,value){
        localStorageService.set(key,JSON.stringify(value));
    };

    session.getItemObject = function(key){
        return JSON.parse(localStorageService.get(key));
    };

    session.removeItem = function(key){
        localStorageService.remove(key);
    };

    session.clearSession = function(){
        localStorageService.clearAll();
    };

    session.isAuthenticated = function(){
        if(session.getItem('user_data')){
            return true;
        }else{
            return false;
        }
    };

    return session;
}
]);
