application.factory('Client',['$http','config', function($http,config) {
    var service={};

    service.create = function(client) {
      return $http.post(config.API_BASE_PATH + '/clients', { client: client });
    };

    service.update = function(client) {
      return $http.put(config.API_BASE_PATH + '/clients/' + client.id, { client: client });
    };

    service.destroy = function(client) {
      return $http.delete(config.API_BASE_PATH + '/clients/' + client.id);
    };

    service.find = function(client) {
      return $http.get(config.API_BASE_PATH + '/clients/' + client.id);
    };

    service.offers = function(client) {
      return $http.get(config.API_BASE_PATH + '/clients/' + client.id + '/offers');
    };

    return service;
}
]);
