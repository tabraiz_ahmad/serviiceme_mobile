application.config(['$stateProvider', '$urlRouterProvider', function ($stateProvider, $urlRouterProvider) {
  $stateProvider
    .state('dev', {
      url: '/dev',
      templateUrl: 'templates/comingSoon.html',
      controller: 'comingSoonCtrl'
    })
    .state('login', {
      url: '/login/:type',
      cache:false,
      templateUrl: 'templates/login.html',
      controller: 'loginCtrl'
    })
    .state('protected', {
      url: '/protected',
      cache:false,
      templateUrl: 'templates/protected.html',
      controller: 'protectedCtrl'
    })
    .state('presentation', {
      url: '/presentation/:type',
      templateUrl: 'templates/presentation.html',
      controller: 'presentationCtrl'
    })
    .state('menu', {
      url: '/menu',
      templateUrl: 'templates/menu.html',
      cache: false,
      controller: 'menuCtrl'
    })
    .state('menu.landing', {
      url: '/landing',
      cache: false,
      views: {
        'main': {
          templateUrl: 'templates/landing.html',
          controller: 'landingCtrl'
        }
      }
    })
    .state('menu.service', {
      url: '/service/:type',
      views: {
        'main': {
          templateUrl: function($stateParams){
            return 'templates/'+$stateParams.type+'.html';
          },
          controller: 'serviceCtrl'
        }
      }
    })
    .state('menu.offerMethod', {
      url: '/offerMethod/:category',
      views: {
        'main': {
          templateUrl: 'templates/offerMethod.html',
          controller: 'offerMethodCtrl'
        }
      }
    })
    .state('menu.offer', {
      url: '/offer/:category/:method',
      views: {
        'main': {
          templateUrl: function($stateParams){
            return 'templates/'+$stateParams.category+'.html';
          },
          controller: 'offerCtrl'
        }
      }
    })

    $urlRouterProvider.otherwise('/protected');

}]);
